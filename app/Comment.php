<?php

namespace StudentHelper;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comment';

    protected $attributes = ['rate' => 0, 'visible' => true];
    protected $fillable = ['post', 'user', 'text',];
    protected $visible = ['id', 'text', 'rate', 'user', 'created_at', 'edited_at'];
}
