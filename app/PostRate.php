<?php

namespace StudentHelper;

use Illuminate\Database\Eloquent\Model;

class PostRate extends Model
{
    protected $table = 'post_rate';
    public $timestamps = false;

    // vote == true => upvote, vote == false => downvote
    protected $attributes = ['vote' => false];
    protected $fillable = ['user', 'post', 'vote'];
    protected $visible = ['user', 'post', 'vote'];
}
