<?php

namespace StudentHelper;



use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'user';
    public $timestamps = false;

    protected $attributes = [ 'rate' => 0];
    protected $fillable = ['name', 'email', 'password'];
    protected $visible = ['name', 'email', 'rate'];


}
