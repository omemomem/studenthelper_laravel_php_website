<?php

namespace StudentHelper;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    protected $table = 'tag';
    public $timestamps = false;

    protected $fillable = ['name'];
    protected $visible = ['name'];
}
