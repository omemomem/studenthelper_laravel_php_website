<?php

namespace StudentHelper;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'post';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $attributes = ['rate' => 0, 'visible' => true, 'ver_comment' => null];
    protected $fillable = ['author', 'title', 'description', 'ver_comment', 'rate',];
    protected $visible = ['title', 'description', 'ver_comment', 'rate', 'created_at', 'updated_at'];
}
