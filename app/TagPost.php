<?php

namespace StudentHelper;

use Illuminate\Database\Eloquent\Model;

class TagPost extends Model
{
    protected $table = 'tag_post';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = ['tag', 'post'];
}
