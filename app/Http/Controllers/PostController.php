<?php

namespace StudentHelper\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use StudentHelper\Comment;
use StudentHelper\Post;
use StudentHelper\PostRate;
use StudentHelper\PostRateController;
use StudentHelper\Tag;
use StudentHelper\TagPost;
use StudentHelper\User;
use StudentHelper\Utility;

class PostController extends Controller
{
    public function index()
    {
        return view('post-index');
    }

    public function create()
    {
        return view('post-create');
    }

    /*
     * show a post with given postid
     */
    public function show($postId, $title)
    {
        $Post = new Post();
        $User = new User();
        $Comment = new Comment();

        // validate if url attributes matches record requested
        if (!Utility::isValidPostAttr($postId, $title)) {
            return 'Oooooops! Page does not exist.. YIKES!';

        }
        $post = $Post->find($postId);
        if ($post->visible == false)
            return 'Post has been deleted';
        $userAuthor = $User->find($post->author);
        $comments = $Comment->where('post', $postId)->where('visible', true)->get();
        foreach ($comments as &$comment) {
            $author = $User->find($comment->user);
            $comment->userName = $author->name;
        }
        unset($comment);
        // attributes passed on to the post-show.blade view
        $showPostAttr = array('author' => $userAuthor->name, 'title' => $post->title,
            'body' => $post->description, 'comments' => $comments, 'rate' => $post->rate, 'id' => $postId, 'watcher' => Auth::id());
        return view('post-show', $showPostAttr);
    }

    /*
     * save a new post via form
     */
    public function store(Request $request)
    {
        //TODO: check if user didnt post more then x posts for the last 24 horus and if such post doesnt already exist [Prevent from using bad words]
        if (!Auth::check()) {
            return redirect('login');
        }
        // regex: \x{0590}-\x{05FF} all the hebrew letters
        $this->validate($request, [
            'title' => array('required', 'min:5', 'max:20', 'regex:/[A-Za-z\x{0590}-\x{05FF} ]/umi'),
            'description' => 'required|min:10|max:1000',
            'tags' => array('required', 'min:3', 'max:100', 'regex:/[\x{0590}-\x{05FF} -]/umi'),
        ]);
        $post = new Post();
        $post->title = $request->title;
        $post->description = $request->description;
        $post->author = Auth::id();
        // store post
        $post->save();
        $postId = $post->id;
        // store tags
        $tagArrId = $this->storeTags(explode(' ', $request->tags));
        // store tag-post connection
        $this->storeTagPost($tagArrId, $postId);

        return redirect()->route('post_show', ['id' => $postId, 'title' => $post->title]);
    }

    /*
     * get array of tags and insert them to the db
     * return array of ids[Integer] of the added tags
     */
    private function storeTags($tagsArr)
    {
        $tagsId = array();
        foreach ($tagsArr as $tag) {
            if (strlen($tag) >= 3) {
                $Tag = new Tag();
                $tagId = $Tag->firstOrCreate(['name' => $tag]);
                $tagsId[] = $tagId->id;
            }
        }
        return $tagsId;
    }

    /*
     * store array of tags given with a post to associate a post with the tags
     */
    private function storeTagPost($tagsArray, $postId)
    {
        for ($i = 0; $i < count($tagsArray); $i++) {
            $TagPost = new TagPost();
            $TagPost->firstOrCreate(['tag' => $tagsArray[$i], 'post' => $postId]);
        }
    }

    /*
     * rate post, $upvote is a boolean, true = upvote, false = downvote
     */
    public function rate($postId, $title, $vote)
    {
        $PostRate = new PostRate();
        $Post = new Post();
        $voteBool = filter_var($vote, FILTER_VALIDATE_BOOLEAN);
        $post = $Post->find($postId);
        // use filter_var($vote, FILTER_VALIDATE_BOOLEAN) to check if $vote is true or false
        if (!Auth::check()) {
            return 'false';
        }
        if (!Utility::isValidPostAttr($postId, $title)) {
            return 'false';
        }
        $tag = $PostRate->where('user', Auth::id())->where('post', $postId)->first();
        // post was voted by user in the past
        if ($tag == null) {
            $PostRate = new PostRate();
            $Post = new Post();
            $PostRate->insert(['user' => Auth::id(), 'post' => $postId, 'vote' => $voteBool]);
            // post is to be upvoted
            if ($voteBool) {
                DB::table('post')->where('id', $postId)->increment('rate');
                DB::table('user')->where('id', $post->author)->increment('rate');
            } else {
                // post is to be downvoted
                $post = $Post->find($postId);
                DB::table('post')->where('id', $postId)->decrement('rate');
                DB::table('user')->where('id', $post->author)->decrement('rate');
            }

        } else {
            //first time user votes this post
            $tagVote = $tag->vote;
            $tag->vote = $voteBool;
            $tag->save();
            // vote was previously an upvote
            if ($tagVote) {
                if (!$tag->vote) {
                    // we decrease one vote for to cancel out his prev upvote and another for his current downvote
                    DB::table('post')->where('id', $postId)->decrement('rate', 2);
                    DB::table('user')->where('id', $post->author)->decrement('rate', 2);
                }
            } else {
                // vote was previously a downvote
                if ($tag->vote) {
                    // we increase one vote for to cancel out his prev downvote and another for his current upvote
                    DB::table('post')->where('id', $postId)->increment('rate', 2);
                    DB::table('user')->where('id', $post->author)->increment('rate', 2);
                }
            }
        }
        // no change was made to the vote
        return back();
    }


}
