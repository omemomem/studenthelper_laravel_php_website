<?php

namespace StudentHelper\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use StudentHelper\Comment;
use StudentHelper\CommentRate;
use StudentHelper\Utility;

class CommentController extends Controller
{
    public function store(Request $request, $postId, $title)
    {
        //TODO: check if user did not spam and if exact comment does not exist [Validate Comment]
        if (!Utility::isValidPostAttr($postId, $title)) {
            return 'Not authorized to comment - Wrong post identifier';
        }
        if (!Auth::check()) {
            return 'Not authorized to comment - Please log in';
        }
        $this->validate($request, [
            'comment' => 'required|min:5|max:400',
        ]);
        $comment = new Comment();
        $comment->post = $postId;
        $comment->user = Auth::id();
        $comment->text = $request->comment;
        $comment->save();
        return back();
    }

    public function rate($commentId, $postId, $title, $vote)
    {
        $CommentRate = new CommentRate();
        $Comment = new Comment();
        $voteBool = filter_var($vote, FILTER_VALIDATE_BOOLEAN);
        $comment = $Comment->find($commentId);
        // use filter_var($vote, FILTER_VALIDATE_BOOLEAN) to check if $vote is true or false
        if (!Auth::check()) {
            return 'false';
        }
        if (!Utility::isValidPostAttr($postId, $title)) {
            return 'false';
        }
        $tag = $CommentRate->where('user', Auth::id())->where('comment', $commentId)->first();
        // post was voted by user in the past
        if ($tag == null) {
            $CommentRate = new CommentRate();
            $CommentRate->insert(['user' => Auth::id(), 'comment' => $commentId, 'vote' => $voteBool]);
            // post is to be upvoted
            if ($voteBool) {
                DB::table('comment')->where('id', $commentId)->increment('rate');
                DB::table('user')->where('id', $comment->user)->increment('rate');
            } else {
                // post is to be downvoted
                DB::table('comment')->where('id', $commentId)->decrement('rate');
                DB::table('user')->where('id', $comment->user)->decrement('rate');
            }

        } else {
            //first time user votes this post
            $tagVote = $tag->vote;
            $tag->vote = $voteBool;
            $tag->save();
            // vote was previously an upvote
            if ($tagVote) {
                if (!$tag->vote) {
                    // we decrease one vote for to cancel out his prev upvote and another for his current downvote
                    DB::table('comment')->where('id', $commentId)->decrement('rate', 2);
                    DB::table('user')->where('id', $comment->user)->decrement('rate', 2);
                }
            } else {
                // vote was previously a downvote
                if ($tag->vote) {
                    // we increase one vote for to cancel out his prev downvote and another for his current upvote
                    DB::table('comment')->where('id', $commentId)->increment('rate', 2);
                    DB::table('user')->where('id', $comment->user)->increment('rate', 2);
                }
            }
        }
        // no change was made to the vote
        return back();
    }

    // TODO: add edit comment function
    public function edit()
    {

    }
}
