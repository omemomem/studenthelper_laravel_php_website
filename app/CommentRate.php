<?php

namespace StudentHelper;

use Illuminate\Database\Eloquent\Model;

class CommentRate extends Model
{
    protected $table = 'comment_rate';
    public $timestamps = false;

    // vote == true => upvote, vote == false => downvote
    protected $attributes = ['vote' => false];
    protected $fillable = ['user', 'comment', 'vote'];
    protected $visible = ['user', 'comment', 'vote'];
}
