<?php

namespace StudentHelper;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use StudentHelper\Post;
use Illuminate\Http\Request;
use StudentHelper\Tag;
use StudentHelper\TagPost;
use StudentHelper\User;

Class Utility
{
    /*
     * validate if a given post id and title matches database record
     * $title is always in the format of dashes instead of whitespaces
     */
    public static function isValidPostAttr($postId, $title)
    {
        if ($postId == null || $title == null)
            return false;
        $Post = new Post();
        $post = $Post->find($postId);
        if ($post != null and $post->exists()) {
            // format post title to match $title
            $postTitle = $post->title;
            ltrim($postTitle);
            rtrim($postTitle);
            $postTitle = str_replace(' ', '-', $postTitle);
            if (strcmp($postTitle, $title) == 0)
                return true;
            return false;
        } else
            return false;

    }

}