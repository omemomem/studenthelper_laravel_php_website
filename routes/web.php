<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', function () {
    return view('home');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/post/create', 'PostController@create');

Route::post('/post/create', 'PostController@store')->name("post_create");

Route::get('post/{id}/{title}', 'PostController@show')->name('post_show');

Route::post('comment/{postId}/{title}', 'CommentController@store')->name('comment_create');

Route::get('post/{postId}/{title}/{vote}', 'PostController@rate')->name('post_rate');

Route::get('comment/{commentId}/{postId}/{title}/{vote}', 'CommentController@rate')->name('comment_rate');

