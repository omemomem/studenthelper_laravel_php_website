@extends('layouts.app')

@section('content')
    <div class='col-md-8 col-md-offset-2'>
        <div class="panel panel-default container">
            <header class="col-md-offset-9 row"><h1>{{$title}}</h1></header>
            <div class="col-md-2 row bg-info">דירוג הפוסט <h4>{{$rate}} </h4></div>
            <br><br><br><br>
            <div class="rate">
                <button>
                    <a href="{{route('post_rate', ['postId'=> $id, 'title' => $title,'vote' => 'true'])}}">upvote</a>
                </button>
                <button>
                    <a href="{{route('post_rate', ['postId'=> $id, 'title' => $title,'vote' => 'false'])}}">downvote</a>
                </button>
            </div>
            <br>
            <div class="body col-md-offset-7">
                {{$body}}
            </div>
            <br>
            <div class="row"><p>{{$author}} הפוסט נכתב על ידי</p></div>
            <br>
        </div>
    </div>
    <div class='col-md-8 col-md-offset-2'>
        <div class="panel panel-default container">
            <div class="text-justify col-md-offset-5">
                <form action="{{ route('comment_create', ['id' => $id, 'title' => $title]) }}" method="POST">
                    {{ csrf_field() }}
                    <p>כתוב את תגובתך כאן</p><br>
                    <textarea name="comment" style="resize:none;"
                              oninput='this.style.height = "";thisphp.style.height = this.scrollHeight + "px"'>
                    </textarea>
                    <br>
                    <button>פרסם תגובה</button>
                    @if ($errors->any())
                        <div class="alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </form>
            </div>
        </div>
        <br>
        <div class="comment">
            @if (count($comments) > 0)
                <div class='col-md-8 col-md-offset-2'>
                    <header><h3></h3></header>
                    @foreach ($comments->all() as $comment)
                        <div class="comment panel panel-default">
                            <div class="col-md-offset-5 row">{{$comment->text}}</div>
                            <br>
                            <div class="panel-info col-md-offset-2 row">
                                דירוג
                                <div class="row col-lg-1">{{$comment->rate}}</div>
                                <br>
                                <button>
                                    <a href="{{route('comment_rate', ['$commentId'=> $comment->id,'postId' =>$id, 'title' => $title,'vote' => 'true'])}}">upvote</a>
                                </button>
                                <button>
                                    <a href="{{route('comment_rate', ['$commentId'=> $comment->id,'postId' =>$id, 'title' => $title,'vote' => 'false'])}}">downvote</a>
                                </button>
                            </div>
                            <div class="col-md-offset-1 row">{{$comment->created_at}}</div>
                            <div class="col-md-offset-1 row">{{$comment->userName}}</div>
                            @if ($comment->user == $watcher)
                                <div class="col-md-offset-1 row"><a>Edit Comment</a></div>
                            @endif
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@endsection