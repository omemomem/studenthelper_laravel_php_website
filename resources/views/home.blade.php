@extends('layouts.app')

@section('content')

    <div class='col-md-8 col-md-offset-2'>
        <div class="panel panel-default">
            <div class="container">
                <div class='col-md-offset-5' id="searchbox" class="centered">
                    <form action="">
                        <input type="radio" name="searchbox-opt" value="tag"> tag<br>
                        <input type="radio" name="searchbox-opt" value="keyword"> keyword<br>
                        <input id="searchbox-input" autocomplete="off"
                               placeholder="כתוב את הנושא שאתה מחפש או מילות מפתח משאלה"
                               size="50" tabindex="-1" type="text" aria-hidden="true"><br>
                        <button class='col-md-offset-2' id="searchbox-submit">חיפוש</button>
                    </form>
                </div>
                <div id=sidebar>
                    <!-- TODO: add popup to notify user he needs to be logged in / register the app -->
                    <button class="col-md-offset-2 button-blue"><a
                                @if (Auth::check())
                                href="{{route('post_create')}}"
                                @else
                                href="{{route('login')}}"
                                @endif>כתוב פוסט
                        </a></button>
                </div>
            </div>
        </div>
    </div>
@endsection
