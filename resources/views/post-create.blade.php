@extends('layouts.app')
@section('content')
    <div class='col-md-8 col-md-offset-2'>
        <div class="panel panel-default container">
            <form action="{{ route('post_create') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} col-md-offset-2">
                    Title: <input type="text" name="title" required value="{{old('title')}}"><br>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} col-md-offset-2">
                    Question: <textarea name="description" required
                                        style="overflow:auto;resize:none">{{old('description')}}</textarea><br>
                </div>

                <p class="col-md-offset-2 alert-info"><b> יש לכתוב את התגיות אך ורק עם אותיות ושימוש ב"-" במקום רווחים למשל אלגברה-לינארית יש להפריד באמצעות רווחים ביו תגיות</b></p>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} col-md-offset-2">
                    Tags: <input type="text" name="tags"
                                 placeholder="write your tags seperated by spaces and with - ex: subject-subject"
                                 required
                                 value="{{old('tags')}}"><br>
                </div>
                <input class="col-md-offset-3" type="submit" value="Submit">
            </form>
            @if ($errors->any())
                <div class="alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
@endsection