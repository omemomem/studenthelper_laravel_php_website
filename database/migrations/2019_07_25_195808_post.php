<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Post extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author');
            $table->foreign('author')->references('id')->on('user')->onDelete('cascade');
            $table->string('title');
            // text body of question
            $table->string('description', 1000)->nullable(false);
            // verified comment by user
            $table->integer('ver_comment')->nullable(true);
            $table->foreign('ver_comment')->references('id')->on('comment');
            $table->integer('rate');
            $table->boolean('visible');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post');
    }
}
